<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<h3>Chess Board using Nested For Loop</h3>

  <div width="1330" height="570" border="1px">
    <?php
    $number=$_POST['number'];
    for($row=1;$row<=$number;$row++)
    {
        echo '<div>;
        for($col=1;$col<=$number;$col++)
        {
            $total=$row+$col;
            if($total%2==0)
            {<?php
                echo "<div height=30px width=30px bgcolor=#FFFFFF></div>"; ?>
            }
            else
            {<?php
                echo "<div height=30px width=30px bgcolor=#000000></div>"; ?>
            }
        }
        echo </div>';
    }
    ?>
  </div>

</body>
</html>